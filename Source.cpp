#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void voice() {}
};

class Dog : public Animal
{
public:
	virtual void voice() override
	{
		cout << "*woof*" << endl;
	}
};

class Cat : public Animal
{
public:
	virtual void voice() override
	{
		cout << "*meow*" << endl;
	}
};

class Bird : public Animal
{
public:
	virtual void voice() override
	{
		cout << "*chirp*" << endl;
	}
};

int main()
{
	Animal* x[3]{};
	x[0] = new Dog;
	x[1] = new Cat;
	x[2] = new Bird;

	for (int i = 0; i < 3; i++)
	{
		x[i]->voice();
	}
	delete* x;

	return 1;
}